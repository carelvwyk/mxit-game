class MxitController < ApplicationController
  include MxitRails::Page
  before_filter :user_management
  before_filter :session_management  

private
  def user_management
    @user = User.find_or_create_by(:auth_key => mxit_id, :auth_provider => :mxit)
    @user.last_activity = Time.now      
  end
  
  def session_management
    if session[:expires_at].nil? || session[:expires_at] <= Time.now # Session doesn't exist or expired
      reset_session
      session[:created] = Time.now
      @user.visit_count += 1
    end
    session[:expires_at] = Time.now + 15.minutes
  end             

  def mxit_id
    raise "No MXit Id Header" if (mxit_params[:mxit_id].nil? && Rails.env.production?)
    mxit_params[:mxit_id] || '123'
  end

  def mxit_login
    raise "No MXit Login Header" if (mxit_params[:mxit_login].nil? && Rails.env.production?)
    mxit_params[:mxit_login] || '123'
  end

  def display_name
    raise "No MXit Display Name Header" if (mxit_params[:display_name].nil? &&
      Rails.env.production?)
    mxit_params[:display_name] || '123'
  end
end