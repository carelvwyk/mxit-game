# Controller involved with profile creation and maintenance

class ProfilesController < GameController
  skip_before_filter :set_username, :only => [:new, :create]
  skip_before_filter :set_plotname, :only => [:new, :create]

  def index
  end

  def show
    if params[:destroy]
      @user.profile.delete
      render 'destroy' and return
    end
    params[:user_id] ||= @user.id
    @profile = User.find(params[:user_id]).profile
    # Update the other player
    @profile.user.update! if @profile.user != @user
    redirect_to root_path unless @profile
  end

  def new
    @profile = Profile.new()
  end

  def create
    @profile = @user.build_profile(params[:profile])
    render :new unless @user.profile.save
  end
end
