# Game controller class makes sure everything is set up properly to play
class GameController < MxitController

  before_filter :set_username
  before_filter :set_plotname

  before_filter :update_user

  def build
    return unless @user.plot
    object_type = params[:object_type]
    resource_type = params[:resource_type]
    if object_type.to_sym == :generator
      @user.plot.build!(:generator, resource_type.to_sym)
    end
    redirect_to :back and return
  end

  def upgrade
    building = @user.plot.building(params[:id])
    building.upgrade! if building
    redirect_to :back and return
  end

  def destroy
    building = @user.plot.building(params[:id])
    building.delete if building
    redirect_to :back and return
  end

private
  def set_username
    redirect_to new_user_profile_path(@user) unless (@user.profile && @user.profile.name)
  end

  def set_plotname
    redirect_to new_user_plot_path(@user) unless (@user.plot && @user.plot.name)
  end

  def update_user
    start = Time.now
    @user.update!
    delta = Time.now - start
    logger.info "Updated #{@user.profile.name} in #{delta} seconds." if @user.profile
  end
end
