# Controller involved with plot creation and maintenance

class PlotsController < GameController
  skip_before_filter :set_plotname, :except => [:index]

  def show
    if params[:destroy]
      @user.plot.delete
      render 'destroy' and return
    end
  end

  def new
    @plot = Plot.new()
  end

  def create
    @plot = @user.build_plot(params[:plot])
    @plot.silos << Silo.default_plasma_silo
    @plot.silos << Silo.default_oxygen_silo
    render :new unless @user.plot.save!
  end
end
