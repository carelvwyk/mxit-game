class Messaging::MessageController < GameController
  before_filter :assign_recipient

  def new
    @message = Messaging::Message.new()
  end

  def create
    @message = Messaging::Message.new(params[:messaging_message].merge(:author_id => @user.id))
    if @message.valid?
      begin
        @conversation = Messaging::Conversation.send_message(@user, @recipient, @message)
        redirect_to messaging_conversation_path(@conversation)
        return
      rescue Messaging::BlockedError => e
        @error_message = e.message
        render :blocked
        return
      end
    else
      render :new
      return
    end
  end

private
  def assign_recipient
    @recipient = User.find(params[:user_id])
  end
end