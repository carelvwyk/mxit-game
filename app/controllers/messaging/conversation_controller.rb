class Messaging::ConversationController < GameController
  def index
    @conversations = @user.conversations.desc(:updated_at)
    # When the user access his inbox, clear the "has_new_messages" flag
    @user.has_new_messages = false
    @user.save!
  end

  def show
    id = params[:id]
    # Make sure the logged in user has access to this conversation
    @conversation = Messaging::Conversation.find(id)
    if not @conversation.conversation_owner == @user
      redirect_to root_path()
      return
    end
    @recipient_name = @conversation.participant.profile.name
    @conversation.has_unread_messages = false
    @conversation.save!
  end
end