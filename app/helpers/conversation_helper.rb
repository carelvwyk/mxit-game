module ConversationHelper
  def format_last_message_time(message_time)
    time_ago = (Time.now - message_time)
    days_ago = (time_ago/1.day).round
    return "#{days_ago}d" if days_ago > 0
    hours_ago = (time_ago/1.hour).round
    return "#{hours_ago}h" if hours_ago > 0
    minutes_ago = (time_ago/1.minute).round
    return "#{minutes_ago}m" if minutes_ago > 0
    seconds_ago = (time_ago/1.second).round
    return "#{seconds_ago}s"
  end
end