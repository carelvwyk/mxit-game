module GameHelper
  def upgrade_link(building)
    if building.upgrading?
      seconds_to_complete = (building.upgrade_completion_time.to_time - Time.now).round
      "#{seconds_to_complete}s left"
    elsif building.next_level
      link_to("Upgrade", game_upgrade_path(id: building.id)).to_s + " (#{building.upgrade_time}s)"
    else
      " Maxed"
    end
  end
end