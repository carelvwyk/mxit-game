class GameObjectFactory
  # Note: Upgrade time is in seconds
  def self.resource_generator(resource_type, level)
    values = {}
    case resource_type
    when :plasma then values = plasma_generator(level)
    when :oxygen then values = oxygen_generator(level)
    else
      return nil
    end
    return nil unless values
    values.merge!({level: level})
    new_resource = ResourceGenerator.new(values)
    if !new_resource.valid?
      raise "#{new_resource.errors.messages}"
    end
    new_resource
  end

private
  # Plasma Generators:
  def self.plasma_generator(level)
    resource_type = :plasma
    values = {}
    case level
      # Level 1:
    when 1 then values = {charge_rate: 1800, build_cost: {:plasma => 100, :oxygen => 0}, upgrade_time: 30}
      # Level 2:
    when 2 then values = {charge_rate: 3600, build_cost: {:plasma => 200, :oxygen => 20}, upgrade_time: 60}
    else
      return nil
    end
    values.merge!({resource_type: resource_type})
  end

  # Oxygen Generators:
  def self.oxygen_generator(level)
    resource_type = :oxygen
    values = {}
    case level
      # Level 1:
    when 1 then values = {charge_rate: 180, build_cost: {:plasma => 100, :oxygen => 0}, upgrade_time: 0}
      # Level 2:
    when 2 then values = {charge_rate: 360, build_cost: {:plasma => 1000, :oxygen => 20}, upgrade_time: 30}
    else
      return nil
    end
    values.merge!({resource_type: resource_type})
  end
end