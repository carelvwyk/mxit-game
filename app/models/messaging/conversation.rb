module Messaging
  class Conversation
    include Mongoid::Document
    include Mongoid::Timestamps # For created_at and updated_at

    field :participant_id,  type: Moped::BSON::ObjectId

    field :blocked,             type: Boolean, default: false
    field :has_unread_messages, type: Boolean, default: true

    belongs_to :conversation_owner, class_name: 'User', inverse_of: :conversations

    embeds_many :messages, class_name: 'Messaging::Message'

    # Indexes
    index({ conversation_owner: 1, participant_id: 1 }, { unique: true, name: "conversation_index" })

    def participant
      User.find(self.participant_id)
    end

    def self.sender_conversation(sender, recipient)
      sender.conversations.find_or_create_by(participant_id: recipient.id)
    end

    def self.recipient_conversation(sender, recipient)
      recipient.conversations.find_or_create_by(participant_id:sender.id)
    end

    def self.send_message(sender, recipient, message)
      raise Messaging::SameUserError, "Cannot send messages to yourself" if (sender == recipient)
      # Find the recipient's version of the conversation
      recipient_conversation = Messaging::Conversation.recipient_conversation(sender, recipient)
      if recipient_conversation.blocked?
        raise Messaging::BlockedError, "#{recipient.profile.name} has blocked you."
      end
      # Find the sender's version of the conversation
      sender_conversation = Messaging::Conversation.sender_conversation(sender, recipient)
      recipient_conversation.messages << message
      recipient_conversation.has_unread_messages = true
      recipient.has_new_messages = true
      sender_conversation.messages << message

      recipient_conversation.save!
      recipient.save!
      sender_conversation.save!
      sender_conversation
    end
  end
end