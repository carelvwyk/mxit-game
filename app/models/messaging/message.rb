module Messaging
  class Message
    include Mongoid::Document
    include Mongoid::Timestamps # For created_at and updated_at

    field :text,         type: String

    # Constraints
    validates_length_of :text, minimum: 2, maximum: 256
    validates_presence_of :author

    # Relationships
    embedded_in :conversation
    belongs_to :author, :class_name => 'User', :inverse_of => nil

    def sent_by?(user)
      self.author == user
    end

  end
end