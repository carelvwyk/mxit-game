class ResourceGenerator < Resource
  include Mongoid::Document

  # Stores the charge rate per hour
  field :charge_rate, type: Float
  # Stores the time this generator was last harvested
  field :last_harvest, type: DateTime, default: ->{ Time.now }

  #--- Upgrade related fields:
  # Current upgrade level
  field :level, type: Integer, default: 1 # Start at level 1
  # Cost to build or upgrade from the previous level to this level
  field :build_cost, type: Hash
  # Time it takes to upgrade from the previous level to this level in seconds
  field :upgrade_time, type: Integer
  # Determines when an upgrade completes
  field :upgrade_completion_time, type: DateTime, default: nil

  validates_presence_of :charge_rate, :level, :build_cost, :upgrade_time

  # Parent document (what this resource generator belongs to)
  embedded_in :plot

  # Harvest resources based on the recharge rate and when last it was harvested
  def harvest!
    harvest_volume = 0
    # If there was an upgrade and that upgrade already completed, calculate the harvest volume before it completed:
    if self.upgrade_completion_time and self.upgrade_completion_time < Time.now
      # Calculate the volume of resource generated before the upgrade and then update generator values
      time_delta = (self.upgrade_completion_time.to_time - self.last_harvest.to_time).seconds
      harvest_volume += time_delta * (self.charge_rate / 60.0 / 60.0)
      self.last_harvest = self.upgrade_completion_time
      update_upgraded_attributes!
    end
    time_delta = Time.now - self.last_harvest # Calculate seconds since the last harvest
    harvest_volume += time_delta * (self.charge_rate / 60.0 / 60.0) # Multiply by the recharge rate divided by seconds per hour
    self.last_harvest = Time.now # Update last harvest time to now (we just harvested)
    self.save! # Save state
    logger.info("\n>>> #{self.resource_type} generator yielded #{harvest_volume} of #{self.resource_type} at #{Time.now}\n")
    {self.resource_type => harvest_volume} # Return the resource type and volume
  end

  def upgrade!
    return false if self.upgrading?
    # Only upgrade if a next level is available
    return false unless self.next_level
    # Before we can begin upgrading, we need to make sure all resources are harvested:
    self.plot.update!
    # Now begin upgrading
    self.upgrade_completion_time = Time.now + self.upgrade_time
    self.save!
  end

  # Tells us whether this generator is currently busy upgrading
  def upgrading?
    return false unless self.upgrade_completion_time
    self.upgrade_completion_time > Time.now
  end

  def next_level
    GameObjectFactory.resource_generator(self.resource_type, self.level+1)
  end

private
  def update_upgraded_attributes!
    self.upgrade_completion_time = nil # Reset upgrade completion time
    new_values = next_level
    return unless new_values
    self.level = new_values.level
    self.build_cost = new_values.build_cost
    self.upgrade_time = new_values.upgrade_time
    self.charge_rate = new_values.charge_rate
  end
end