class User
  include Mongoid::Document
  # Authentication
  field :auth_provider,     type: Symbol
  field :auth_key,          type: String

  validates_presence_of     :auth_provider
  validates_presence_of     :auth_key
  validates_inclusion_of    :auth_provider, in: [ :mxit ]

  # Activity Tracking
  field :joined,            type: DateTime, default: ->{ Time.now }
  field :last_activity,     type: DateTime, default: -> { Time.now }
  field :visit_count,       type: Integer,  default: 1

  # Helper fields
  field :has_new_messages, type: Boolean, default: false

  embeds_one :profile
  embeds_one :plot

  has_many :conversations, :class_name => 'Messaging::Conversation', :inverse_of => :conversation_owner

  # Indexes
  index({ auth_key: 1, auth_provider: 1 }, { unique: true, name: "auth_index" })

  def update!
    self.plot.update! if self.plot
  end
end
