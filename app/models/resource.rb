class Resource
  include Mongoid::Document
  # Stores the type of resource
  field :resource_type, type: Symbol

  validates_inclusion_of :resource_type, in: [ :plasma, :oxygen ]
end
