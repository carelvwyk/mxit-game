class Silo < Resource
  include Mongoid::Document

  # Stores the current level of a silo (starts at 0.0)
  field :level, type: Float, default: 0.0
  # Stores the max capacity of a silo
  field :capacity, type: Float

  # Make sure that a plot can only have one type of silo
  validates_uniqueness_of :resource_type # Only checked within parent doc

  # Parent document (what the silo belongs to)
  embedded_in :plot

  # Store harvested material in this silo (if there's more than max capacity, use max capacity)
  def store! (harvest_volume)
    self.level = [self.level + harvest_volume, self.capacity].min
    self.save!
  end

  # Create an example plasma silo
  def self.default_plasma_silo
    Silo.new(resource_type: :plasma, level: 0, capacity: 10000)
  end

  def self.default_oxygen_silo
    Silo.new(resource_type: :oxygen, level: 0, capacity: 100)
  end
end