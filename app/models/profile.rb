class Profile
  include Mongoid::Document
  # Stores the name of the player
  field :name,          type: String

  # Player name validation (Make sure it is unique and correct length)
  validates_uniqueness_of   :name, :case_sensitive => false
  validates_length_of       :name, minimum: 2, maximum: 20

  # The parent document (what the profile belongs to)
  embedded_in :user

  # Indexes (database optimization)
  index({ name: 1 }, { unique: true, name: "profile_name_index" })
end
