class Warehouse
  include Mongoid::Document

  embedded_in :plot

  embeds_many :silos
end