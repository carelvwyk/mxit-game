class Plot
  include Mongoid::Document
  # Stores the name of the plot
  field :name,          type: String

  # Plot name validation (Make sure it is unique and correct length)
  validates_uniqueness_of   :name, :case_sensitive => false
  validates_length_of       :name, minimum: 2, maximum: 20

  # The parent document (what the plot belongs to)
  embedded_in :user

  # Child documents (stuff that belongs to the plot)
  embeds_many :silos
  embeds_many :resource_generators

  # Indexes (database optimization)
  index({ name: 1 }, { unique: true, name: "plot_name_index" })

  def building(building_id)
    self.resource_generators.find(building_id) || self.silos.find(building_id)
  end

  def build!(building_type, resource_type)
    if building_type == :generator
      resource_generators.push(GameObjectFactory.resource_generator(resource_type, 1))
      self.save!
    end
  end

  # Do whatever needs to be done on this plot
  def update!
    # Harvest resources from all resource generators on this plot
    harvest = harvest_resources! # This ends up in the form {plasma: 10, oxygen: 20, etc...}
    # Store resources (this can be moved to a warehouse model later)
    harvest.each do |resource_type, harvest_volume| # This says "For each harvested resource, get it's type and harvest size"
      silo = silos.find_by(:resource_type => resource_type) # Find the silo that stores a specific type of resource
      silo.store! (harvest_volume) if silo # If the silo is found, store the harvest volume inside it
    end
  end

private
  # Loops through all resource generators on this plot and see how much resources they generated since last harvest
  def harvest_resources!
    harvested_resources = {}
    self.resource_generators.each do |rg|
      harvest = rg.harvest! # Harvest!
      harvested_resources.merge!(harvest) { |k, v1, v2| v1+v2 } # Combine harvested resources into a single harvest object for use later
    end
    harvested_resources
  end
end
