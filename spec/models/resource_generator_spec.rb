require 'spec_helper'

describe ResourceGenerator do
  it "harvests resources" do
    user = User.new(name: 'Some User')
    plot = Plot.new(name: 'Some plot', user: user)
    plasma_generator = ResourceGenerator.new(plot: plot, :resource_type => :plasma, :charge_rate => 1800, :last_harvest => Time.now) # charge rate is per hour

    before = Time.now
    after = before + 1.hours

    expected_harvest = (after.to_time - before.to_time) * (plasma_generator.charge_rate / 60.0 / 60.0)

    Time.stub(:now).and_return(before)
    plasma_generator.harvest!
    Time.stub(:now).and_return(after)

    expect(plasma_generator.harvest!).to eq({plasma: expected_harvest})
  end

  it "upgrades and harvests resources" do
    user = User.new(name: 'Some User')
    plot = Plot.new(name: 'Some plot', user: user)
    plasma_generator = GameObjectFactory.resource_generator(:plasma, 1)
    plasma_generator.plot = plot
    upgraded_generator = GameObjectFactory.resource_generator(:plasma, 2)
    before = Time.now
    added_seconds = 40
    after = before + upgraded_generator.upgrade_time.seconds + added_seconds.seconds

    expected_harvest = upgraded_generator.upgrade_time * (plasma_generator.charge_rate / 60.0 / 60.0)
    expected_harvest += added_seconds * (upgraded_generator.charge_rate / 60.0 / 60.0)

    Time.stub(:now).and_return(before)
    plasma_generator.harvest!
    plasma_generator.upgrade!
    Time.stub(:now).and_return(after)

    expect(plasma_generator.harvest!).to eq({plasma: expected_harvest})
  end
end
