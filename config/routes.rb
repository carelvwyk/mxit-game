MxitGame::Application.routes.draw do
  resources :users, :only => [:index] do
    resource :profile, :only => [:index, :new, :create, :show]
    resource :plot, :only => [:new, :create, :show]

    namespace :messaging do
      resources :message, :only => [:new, :create]
    end
  end
  get '/profile', to: 'profiles#show' # current user's profile

  namespace :messaging do
    # Conversations are created when new messages are sent
    resources :conversation, :only => [:index, :show]
    match 'inbox', to: 'conversation#index'
  end

  # Game actions:
  get '/game/build', to: 'game#build'
  get '/game/destroy', to: 'game#destroy'
  get '/game/upgrade', to: 'game#upgrade'

  #get '/temp/second' => 'temp#second'
  root to: 'home#index'
  #root :to => 'temp#first'
end
